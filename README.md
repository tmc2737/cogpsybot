![](https://bytebucket.org/tmc2737/cogpsybot/raw/10e7fe7d9c39bce2a64bccc8f5cfde4d9955cbfa/cogpsy.png?token=b334a0a69fb3f3d96ee4fea4e5a63fcffe92990d)

# @cogpsybot (Twitter)

This bot scrapes different websites for new releases in journal articles related to Cognitive Psychology. Currently, we are scraping from:
- Acta Psychologica
- Applied Research in Memory and Cognition
- Brain and Cognition
- Bulletin of the Psychonomic Society
- Cognition
- Cognitive Neuropsychology 
- Cognitive Neuroscience
- Cognitive Psychology
- Consciousness and Cognition
- Journal of Cognitive Psychology
- Journal of Experimental Psychology: Learning, Memory, and Cognition
- Journal of Memory and Language
- Language, Cognition and Neuroscience 
- Learning & Behavior
- Memory
- Memory & Cognition
- Neuropsychologica
- Neuropsychology
- The Quarterly Journal of Experimental Psychology
- Topics in Cognitive Science
- Trends in Cognitive Sciences 
- Visual Cognition

We will be adding to the list as the code becomes more complex. Target journal companies include Elsevier, Taylor and Francis, Springer, and Routledge.

The cogpsybot is the first iteration of a series of Twitterbots dedicated to mining journal websites and is directly related to @cogagingbot and @quantpsybot.

Follow us on Twitter ([@cogpsybot](http://www.twitter.com/cogpsybot))!


***

###### UPDATE (08/15/16):
- We will be updating the code to delay Tweets. Many publishing companies publish their newest articles in bulk, which causes the bots to post at once, potentially violating Twitter's API guidelines. It's also annoying to see 10 tweets published at once.
